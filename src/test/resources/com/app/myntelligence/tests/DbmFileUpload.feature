@ui @bean-DbmFileUpload
Feature: Upload file with Campaigns in DBM

  As reporting-dev user
  I need possibility to Upload file with Campaigns
  Also review and manage this campaigns

  Background:
    When I login to google account
    Then I go to page "https://displayvideo.google.com"

  Scenario: File upload
    When I search advertiser by ID "2057233"
    Then I can see list of campaigns for Myntelligence
    Given I upload file "SDF-Campaigns - SDF-Campaigns.csv"
#    Then I can see uploaded campaigns on page




