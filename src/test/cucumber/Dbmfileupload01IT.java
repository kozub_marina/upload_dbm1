import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true,
    features = {"classpath:com/app/myntelligence/tests/DbmFileUpload.feature"},
    format = {"json:target/cucumber-parallel/1.json", "pretty"},
    monochrome = true,
    tags = {"@ui", "@bean-DbmFileUpload"},
    glue = { "com.app.myntelligence.tests" })
public class Dbmfileupload01IT {
}