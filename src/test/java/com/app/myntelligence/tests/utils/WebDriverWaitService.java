package com.app.myntelligence.tests.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverWaitService {

    private final WebDriverWait driver;

    public WebDriverWaitService(WebDriverWait driver) {
        this.driver = driver;
    }

    public WebElement getElementWhenDisplayed(By locator) {
        return driver.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

}
