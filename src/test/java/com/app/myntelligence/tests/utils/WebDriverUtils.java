package com.app.myntelligence.tests.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;


public class WebDriverUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverUtils.class);
    private static final String SELENIUM_HUB_ADDRESS = "http://%s/wd/hub";
    private static final String SELENIUM_BROWSER = "selenium.browser";
    private static final String SELENIUM_SERVER = "selenium.server";
    private static final String BROWSER_VERSION = "browser.version";
    private static final String PLATFORM_NAME = "platform.name";
    private static final String CHROME_BROWSER = "chrome";
    private static WebDriver webDriver;

    public static WebDriver getCurrentDriver() {
        WebDriverUtils webDriverUtils = new WebDriverUtils();
        String seleniumServer = System.getProperty(SELENIUM_SERVER);
        String seleniumBrowser = System.getProperty(SELENIUM_BROWSER);
        webDriver = webDriverUtils.getSeleniumDriver(seleniumServer, seleniumBrowser);
        return webDriver;
    }

    public static WebDriverWait webDriverWait() {
        return new WebDriverWait(webDriver, 60);
    }

    private WebDriver getSeleniumDriver(String seleniumServer, String seleniumBrowser) {
        WebDriver driver = null;
        try {
            if (StringUtils.isBlank(seleniumServer)) {
                driver = getLocalDriver(seleniumBrowser);
            } else {
                driver = getRemoteDriver(seleniumServer, seleniumBrowser);
            }
        } catch (MalformedURLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return driver;
    }

    private WebDriver getRemoteDriver(String seleniumServer, String seleniumBrowser) throws MalformedURLException {
        LOGGER.debug("Create remote driver for browser: " + seleniumBrowser + " on " + seleniumServer);
        DesiredCapabilities capabilities = getDesiredCapabilities(seleniumBrowser);
        seleniumServer = String.format(SELENIUM_HUB_ADDRESS, seleniumServer);
        RemoteWebDriver driver = new RemoteWebDriver(new URL(seleniumServer), capabilities);
        driver.setFileDetector(new LocalFileDetector());
        return driver;
    }

    private WebDriver getLocalDriver(String seleniumBrowser) {
        LOGGER.debug("Create local driver for browser: " + seleniumBrowser);
        switch (seleniumBrowser) {
            case CHROME_BROWSER:
                return new ChromeDriver();
            default:
                return new FirefoxDriver();
        }
    }

    private DesiredCapabilities getDesiredCapabilities(String seleniumBrowser) {
        String platformName = System.getProperty(PLATFORM_NAME);
        String browserVersion = System.getProperty(BROWSER_VERSION);
        LOGGER.debug("Set remote platform <" + platformName + "> and browser version <" + browserVersion + ">");
        return new DesiredCapabilities(seleniumBrowser, browserVersion, Platform.fromString(platformName));
    }

    public static void scrollDownPage() {
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        System.out.println(webDriver.findElement(By.cssSelector(".scrollable-view")).getText());
        executor.executeScript("arguments[0].scrollTop = arguments[1];", webDriver.findElement(By.cssSelector(".scrollable-view")), 500);
    }

}
