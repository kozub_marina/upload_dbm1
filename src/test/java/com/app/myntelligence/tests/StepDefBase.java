package com.app.myntelligence.tests;


import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public abstract class StepDefBase {
    private static final String HOSTNAME = "hostname";
    private static final int DEFAULT_TIMEOUT = 20;

    @Autowired(required = false)
    protected WebDriver webDriver;

    private void goToPath(String path) {
        webDriver.get(getObligatorySystemProperty(HOSTNAME).concat(path));
    }

    protected void navigateBack() {
        webDriver.navigate().back();
    }

    protected void navigateTo(String path) {

        String url = getOptionalSystemProperty("hostname") + path;
        if (url.equals(getCurrentPath())) {
            reloadPage();
        }
        goToPath(path);
    }

    protected void waitUntilElementEnabled(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected void waitUntilElementPresence(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void waitUntilTextToBePresentInElement(WebElement element, String text) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    protected void assertElementVisible(By locator) {
        Wait<WebDriver> wait = new FluentWait<>(webDriver)
                .withTimeout(3, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected WebElement getElementWhenVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitUntil(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitUntilElementInvisible(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    protected void waitUntilElementVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOf(element));
    }


    protected void waitUntilElementVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitUntilNumberOfElementsToBe(By locator, final int numbers) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.numberOfElementsToBe(locator, numbers));
    }


    protected WebElement waitUntilElementToBeClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        return wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitRedirectTo(String path) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until(ExpectedConditions.urlMatches(Pattern.quote(path) + "."));
    }

    protected void waitUntilElementToBeSelected(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeSelected(element));
    }

    protected String getCurrentPath() {
        return webDriver.getCurrentUrl();
    }

    protected void reloadPage() {
        webDriver.navigate().refresh();
    }

    protected String getObligatorySystemProperty(String name) {
        String property = System.getProperty(name);
        if (StringUtils.isBlank(property)) {
            throw new IllegalArgumentException("System property [" + name + "] was not specified");
        }
        return property;
    }


    protected String getOptionalSystemProperty(String name) {
        return System.getProperty(name);
    }

    protected void waitAllHTTPRequestsDone() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {

        }
    }

    protected void waitUntilAttributeValueEquals(WebElement webElement, String attributeName, String attributeValue) {
        WebDriverWait wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        wait.until((ExpectedCondition<Boolean>) driver -> {
            String enabled = webElement.getCssValue(attributeName);
            return enabled.equals(attributeValue);
        });
    }


}
