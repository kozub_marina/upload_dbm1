package com.app.myntelligence.tests.spring;

import com.app.myntelligence.tests.utils.WebDriverUtils;
import com.app.myntelligence.tests.utils.WebDriverWaitService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.TimeUnit;

@Configuration
@ComponentScan("com.app.myntelligence.tests")
public class SpringConfiguration {

    private static final int WEB_DRIVER_WAIT_DEFAULT_TIMEOUT = 20;
    private static final int DEFAULT_TIMEOUT = 20;
    private static final int DEFAULT_IMPLICITLY_TIMEOUT = 5;

    @Profile("ui")
    @Bean(destroyMethod = "quit")
    public WebDriver webDriver() {
        WebDriver webDriver = WebDriverUtils.getCurrentDriver();
        webDriver.manage().timeouts().pageLoadTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        webDriver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICITLY_TIMEOUT, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        return webDriver;
    }

    @Profile("ui")
    @Bean()
    public WebDriverWait webDriverWait(WebDriver driver) {
        return new WebDriverWait(driver, WEB_DRIVER_WAIT_DEFAULT_TIMEOUT);
    }

    @Profile("ui")
    @Bean()
    public WebDriverWaitService webDriverWaitService(WebDriverWait driverWait) {
        return new WebDriverWaitService(driverWait);
    }
}
