package com.app.myntelligence.tests.ui;


import com.app.myntelligence.tests.StepDefBase;
import com.app.myntelligence.tests.ui.page.DbmFileUploadPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Profile("ui")
@Component("DbmFileUpload")
public class DbmFileUploadStepDef extends AdminUIStepDef {

    public static class UniqueDbmFileUploadStepDef extends StepDefBase {

        private final static String UPLOAD_FILE_PATH = "src/test/resources/file/";

        @Autowired
        @Qualifier("DbmFileUpload")
        private DbmFileUploadStepDef stepDef;

        @Autowired
        private DbmFileUploadPage dbmFileUploadPage;

        private String getAbsoluteFilePath(String fileName) throws IOException {
            return new File(getFilePath(fileName)).getAbsolutePath();
        }

        private String getFilePath(String fileName) {
            return UPLOAD_FILE_PATH + fileName;
        }

        @Given("^I upload file \"([^\"]*)\"$")
        public void uploadFile(String file) throws InterruptedException, IOException, AWTException {

            dbmFileUploadPage.getOptionsIcon().click();
            waitUntilElementVisible(By.tagName("material-select-item"));
            dbmFileUploadPage.getUploadAction().click();
            waitUntilElementVisible(By.xpath("//strong[text()='Upload structured data files']"));
            dbmFileUploadPage.getBrowseButton().click();

            StringSelection ss = new StringSelection(getAbsoluteFilePath(file));
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            waitUntilElementVisible(By.xpath("//div[contains(@class, 'file-name')]"));
            dbmFileUploadPage.getUploadFileButton().click();
            List<WebElement> allFiles = webDriver.findElements(By.xpath("//div[text()='Download Files']"));
            for (WebElement element : allFiles) {
                waitUntilElementVisible(element);
            }
            Thread.sleep(5000);
            //waitUntilElementInvisible(By.xpath(""));
            dbmFileUploadPage.getCloseDownloadButton().click();


        }

        @When("^I search advertiser by ID \"([^\"]*)\"$")
        public void searchAdvertiserById(String id) throws Throwable {

            dbmFileUploadPage.getSearchIcon().click();
            WebElement inputField = webDriver.switchTo().activeElement();
            inputField.sendKeys(id);
            waitUntilElementPresence(By.tagName("global-search-result"));
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);


        }

        @Then("^I can see list of campaigns for Myntelligence$")
        public void iCanSeeListOfCampaignsForMyntelligence() {
            assertElementVisible(By.xpath("//name-id-cell"));
        }

    }


}
