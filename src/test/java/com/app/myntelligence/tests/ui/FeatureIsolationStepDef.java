package com.app.myntelligence.tests.ui;


import com.app.myntelligence.tests.StepDefBase;
import cucumber.api.Scenario;

public abstract class FeatureIsolationStepDef extends StepDefBase{

    public void beforeScenario(Scenario scenario) throws Exception {

    }

    public void afterScenario(Scenario scenario) {

    }
}
