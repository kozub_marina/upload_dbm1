package com.app.myntelligence.tests.ui.page;

import com.app.myntelligence.tests.Page;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Page
public class DbmFileUploadPage {

    @FindBy(xpath = "//material-button[contains(@class, 'browse-upload-button')]/material-ripple")
    private WebElement browseButton;

    @FindBy(xpath = "//material-button[div/material-icon[@icon='search']]")
    private WebElement searchIcon;

    @FindBy(xpath = "//material-menu[contains(@class, 'farRightDropdown')]")
    private WebElement optionsIcon;

    @FindBy(xpath = "//span[text()='Upload']")
    private WebElement uploadAction;

    @FindBy(xpath = "//material-button[contains(@class, 'upload-button')]/material-ripple")
    private WebElement uploadFileButton;

    @FindBy(xpath = "//material-button[contains(@class, 'close-button')]")
    private WebElement closeDownloadButton;

    public WebElement getCloseDownloadButton() {
        return closeDownloadButton;
    }

    public WebElement getUploadFileButton() {
        return uploadFileButton;
    }

    public WebElement getUploadAction() {
        return uploadAction;
    }

    public WebElement getOptionsIcon() {
        return optionsIcon;
    }

    public WebElement getSearchIcon() {
        return searchIcon;
    }

    public WebElement getBrowseButton() {
        return browseButton;
    }
}
