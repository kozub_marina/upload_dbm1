package com.app.myntelligence.tests.ui.page;

import com.app.myntelligence.tests.Page;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Page
public class GoogleLoginAccountPage {

    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailInput;

    @FindBy(id="identifierNext")
    private WebElement nextButton;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordInput;

    @FindBy(id="passwordNext")
    private WebElement nextButtonPasswordStep;

    public WebElement getNextButtonPasswordStep() {
        return nextButtonPasswordStep;
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public WebElement getEmailInput() {
        return emailInput;
    }

    public WebElement getNextButton() {
        return nextButton;
    }
}
