package com.app.myntelligence.tests.ui;

import com.app.myntelligence.tests.spring.SpringConfiguration;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class UIFeatureIsolatedStepDefDelegate extends UIFeatureIsolatedStepDef {

    public UIFeatureIsolatedStepDef getStepDef() {
        return (UIFeatureIsolatedStepDef) FeatureIsolationProvider.getIsolatedStepDef();
    }

    @Given("^I navigate to page \"([^\"]*)\"$")
    public void i_navigate_to_page(String path) {
        getStepDef().i_navigate_to_page(path);
    }

    @Given("^I navigate to page \"([^\"]*)\" and wait until the page fully loaded$")
    public void i_navigate_to_page_and_wait_until_the_page_is_fully_loaded(String path) throws InterruptedException {
        getStepDef().i_navigate_to_page(path);
        getStepDef().iWaitUntilThePageIsFullyLoaded();
    }

    @Then("^I wait until the page is fully loaded$")
    public void i_wait_until_the_page_is_fully_loaded() throws InterruptedException {
        getStepDef().iWaitUntilThePageIsFullyLoaded();
    }

    @Then("^I redirect to page \"([^\"]*)\"$")
    public void i_redirect_to_page(String path) {
        getStepDef().i_redirect_to_page(path);
    }

    @Given("^I press 'Back' browser button$")
    public void i_press_back_browser_button() {
        getStepDef().i_press_back_browser_button();
    }

    @Then("^I can see page \"([^\"]*)\"$")
    public void iCanSeePage(String page) throws Throwable {
        getStepDef().iCanSeePage(page);
    }

    @When("^I login to google account$")
    public void iLoginToGoogleAccount() throws Throwable {
        getStepDef().loginGoogleAccount();
    }

    @Then("^I go to page \"([^\"]*)\"$")
    public void iGoToPage(String page) throws Throwable {
        getStepDef().iGoToPage(page);
    }




}
