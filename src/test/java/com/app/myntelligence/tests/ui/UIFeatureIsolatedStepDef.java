package com.app.myntelligence.tests.ui;


import com.app.myntelligence.tests.ui.page.GoogleLoginAccountPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

import static org.junit.Assert.assertTrue;

@Profile("ui")
public abstract class UIFeatureIsolatedStepDef extends FeatureIsolationStepDef {

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String HOST_NAME = "hostname";

    @Autowired
    private WebDriver webDriver;


    @Autowired
    private GoogleLoginAccountPage googleLoginAccountPage;

    public void i_navigate_to_page(String path) {
        navigateTo(path);
    }

    public void iWaitUntilThePageIsFullyLoaded() throws InterruptedException {
        Thread.sleep(3000);

    }

    public void i_redirect_to_page(String path) {
        waitRedirectTo(path);
        assertTrue(getCurrentPath().contains(path));
    }

    public void i_press_back_browser_button() {
        navigateBack();
    }

    public void iCanSeePage(String page) throws Throwable {
        waitRedirectTo(page);
    }


    public void loginGoogleAccount() throws Throwable {
        webDriver.navigate().to(getObligatorySystemProperty(HOST_NAME));
        iWaitUntilThePageIsFullyLoaded();
        googleLoginAccountPage.getEmailInput().sendKeys(getObligatorySystemProperty(LOGIN));
        googleLoginAccountPage.getNextButton().click();
        waitUntilElementInvisible(By.id("identifierNext"));
        googleLoginAccountPage.getPasswordInput().sendKeys(getObligatorySystemProperty(PASSWORD));
        googleLoginAccountPage.getNextButtonPasswordStep().click();
        waitUntilElementInvisible(By.id("passwordNext"));

    }

    public void iGoToPage(String page) throws Throwable {
        webDriver.navigate().to(page);
        iWaitUntilThePageIsFullyLoaded();
    }


}
