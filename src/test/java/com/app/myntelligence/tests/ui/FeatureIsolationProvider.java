package com.app.myntelligence.tests.ui;


import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.Objects;

public class FeatureIsolationProvider {

    private static final String BEAN_NAME_PREFIX = "@bean-";
    private static final ThreadLocal<IsolationStepDefItem> FEATURE_ISOLATION_STEP_DEF = new ThreadLocal<>();

    @Autowired
    private ApplicationContext context;

    public static FeatureIsolationStepDef getIsolatedStepDef() {
        IsolationStepDefItem isolationStepDefItem = FEATURE_ISOLATION_STEP_DEF.get();
        if (Objects.nonNull(isolationStepDefItem)) {
            return isolationStepDefItem.getFeatureIsolationStepDef();
        }

        throw new RuntimeException("No isolation step def!");
    }

    @Before
    public synchronized void beforeScenario(Scenario scenario) throws Exception {
        String name = getFeatureBeanName(scenario);

        FeatureIsolationStepDef bean;
        if (StringUtils.isBlank(name)) {
            bean = context.getBean(FeatureIsolationStepDef.class);
        } else {
            bean = context.getBean(name, FeatureIsolationStepDef.class);
        }

        bean.beforeScenario(scenario);
        FEATURE_ISOLATION_STEP_DEF.set(new IsolationStepDefItem(name, bean));
    }


    @After
    public synchronized void afterScenario(Scenario scenario) {
        String name = getFeatureBeanName(scenario);
        IsolationStepDefItem isolationStepDefItem = FEATURE_ISOLATION_STEP_DEF.get();
        if (StringUtils.equals(name, isolationStepDefItem.getName())) {
            if (Objects.nonNull(isolationStepDefItem.getFeatureIsolationStepDef())) {
                isolationStepDefItem.getFeatureIsolationStepDef().afterScenario(scenario);
            }
        }
    }

    private String getFeatureBeanName(Scenario scenario) {
        for (String annotation : scenario.getSourceTagNames()) {
            if (StringUtils.startsWith(annotation, BEAN_NAME_PREFIX)) {
                return StringUtils.removeStart(annotation, BEAN_NAME_PREFIX);
            }
        }

        return null;
    }

    private static class IsolationStepDefItem {

        private final String name;
        private final FeatureIsolationStepDef featureIsolationStepDef;

        IsolationStepDefItem(String name, FeatureIsolationStepDef featureIsolationStepDef) {
            this.name = name;
            this.featureIsolationStepDef = featureIsolationStepDef;
        }

        String getName() {
            return name;
        }

        FeatureIsolationStepDef getFeatureIsolationStepDef() {
            return featureIsolationStepDef;
        }
    }

}
